# Kenneth Christian Mallari

I am currently a Flutter Development Trainee in FFUF Manila. I have a 4 year experience as a Small Business Consultant in a BPO company. We engage with small business owners, accountants and bookkeepers and we help them in finding the right accounting software solution for their business needs.

I am also a Full-stack web developer using MERN stack. I love watching Netflix and YouTube travel vlogs in my free time. I am looking forward to travel the world in the near future.

# Contact Information

LinkedIn: [https://www.linkedin.com/in/kenneth-christian-mallari-620994126/](https://www.linkedin.com/in/kenneth-christian-mallari-620994126/)

Website: [https://mallari-flutter-portfolio.herokuapp.com/#/](https://mallari-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Development Trainee

FFUF Manila, July 2021 to present

- Corporate Training for Flutter Development

## Small Business Consultant

Teletech, from November 2016 to January 2021

- Help small business owners, accountants, bookkeepers in finding the right accounting solution for their needs.

# Skills

## Technical Skills

- Web Development
- Programming

## Soft Skills

- Able to collaborate with people
- Systematic

# Education

## BS Information Technology

Don Honorio Ventura State University, 2012-2016
